peer lifecycle chaincode queryinstalled >&log.txt
    cat log.txt
    PACKAGE_ID=$(sed -n "/mycc_1/{s/^Package ID: //; s/, Label:.*$//;p;}" log.txt )
    echo PackageID is "$PACKAGE_ID"

peer lifecycle chaincode approveformyorg --channelID workspace --name mycc --version 1.0 --init-required --package-id $PACKAGE_ID --sequence 1 \
--tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem

peer lifecycle chaincode checkcommitreadiness --channelID workspace --name mycc --version 1.0 --sequence 1 --output json --init-required


#  peer lifecycle chaincode approveformyorg -o localhost:7060 \
#     --channelID workspace --name mycc --version 1 \
#     --init-required --package-id "$PACKAGE_ID" --sequence 1
peer lifecycle chaincode commit -o orderer1.workspace:7050 --channelID workspace --name mycc --version 1.0 --sequence 1 --init-required --peerAddresses  peer1.developers.workspace:7051 \
--tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem \
--tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt


peer lifecycle chaincode querycommitted --channelID workspace --name mycc  
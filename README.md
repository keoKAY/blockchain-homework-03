ReadME....


1. To Ensure that there is no related containers are running or the crypto-config or channel-artifact already existed run this command stop-remove-containers.sh  

2. To Create the crypto-config and channel-artifact run run.sh command

3. After all the orderer and peers are running : docker exec -it cli bash

    3.1  Execute /scripts/devPeer1.sh first in order to export necessary parth ,create workspace channel and join peer to channel

    3.2 Execute the rest of the script in the script folder ( accountPeer1.sh,hrPeer1.sh,marketingPeer1.sh)

    3.3 Installing chaincode process will be done in the peer1, so be sure to import the necessaary path below : 

    export CHANNEL_NAME=workspace
    export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/users/Admin@developers.workspace/msp
    export CORE_PEER_ADDRESS=peer1.developers.workspace:7051
    export CORE_PEER_LOCALMSPID="Org1MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt



4.  The next step is to install chaincode , all we need to do is to run the below script (located in chaincode-script folder) in this orderer : 
    4.1 install-chaincode.sh
    4.2 approve-chaincode.sh 
    4.3 commit-chaincode.sh 


6. The next step is to setup the explorer by running the docker-compose up -d command that is located in the explorer folder. 
    6.1 Credentail to login into this project explorer:
        IP Address: http://172.105.155.131:8080/
        Username  : exploreradmin
        Password  : exploreradminpw 

7. To setup the grafana just run the docker-compose up -d command in the test-network/minor and logging/monitoring directory 
    7.1 Credential to login into this project grafana : 
        IP Address: http://172.105.155.131:8080/
        Username  : admingrafana
        Password  : admingrafanapw 
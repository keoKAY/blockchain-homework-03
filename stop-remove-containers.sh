docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

docker volume rm $(docker volume ls -q)

rm -rf channel-artifacts
rm -rf crypto-config